<?php
 /**
  * Class     ApiContoller.php
  * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_PunctisCustomization_ApiController extends Mage_Core_Controller_Front_Action{

    /** @var Arvato_Core_Helper_Log log */
    private $log;

    public function preDispatch()
    {
        // init log
        $this->log = Mage::helper('arvato_core/log');

        //check key
        /** @var Bitbull_PunctisCustomization_Helper_Data $helper */
        $helper = Mage::helper('bitbull_punctiscustomization');

        if($helper->isSegmentationRouteKeyRequired()){
            $key = $this->getRequest()->getPost('key', false);
            if(!$helper->checkSegmentationRouteKey($key)){

                $this->log->info('Requested Punctis custom api with wrong key: '.$key, $this);
                //not send any message about wrong key, only redirect to home
                $this->setFlag('', 'no-dispatch', true);
                $this->getResponse()->setRedirect(Mage::getBaseUrl(), 401);
                return;
            }
        }
        return parent::preDispatch();
    }

    public function customerSegmentationAction(){
        //this action accept only post action
        if( !$this->getRequest()->isPost())
        {
            $this->log->info('Requested Punctis custom api with wrong method, required POST but used '. $this->getRequest()->getMethod(),$this);
            $this->getResponse()->setRedirect( Mage::getBaseUrl(), 400 );
            return;
        }

        $userEmail = $this->getRequest()->getPost('email', false);
        $type = $this->getRequest()->getPost('type', false);

        if($userEmail && $type){

            /** @var Mage_Customer_Model_Customer $customer */
            $customer = Mage::getModel('customer/customer')->loadByEmail($userEmail);
            if($customer->getId()){

                try{
                    $mgmValue = Bitbull_PunctisCustomization_Model_Attribute_Source_MgmType::getInviteValueByCode($type);

                    $customer->setData(Bitbull_PunctisCustomization_Model_Attribute_Source_MgmType::OPTION_NAME, $mgmValue);
                    $customer->save();
                    $response = array('success'=>'updated customer');

                    $this->log->info('User with id: '.$customer->getId().' was updated correctly for segmentation, setted '.
                        Bitbull_PunctisCustomization_Model_Attribute_Source_MgmType::OPTION_NAME. ' attribute to value:'.
                        $type, $this);

                }catch (Exception $e){
                    $this->log->error($e->getMessage(), $this, $e);
                    $response = array('error'=>'There was a problem with your the request');
                }
            }else{
                $this->log->info('Customer with email: '. $userEmail.' not found',$this);

                $response = array('error'=>'customer not found');
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            return;
        }
        $this->log->info('Redirect user to home for wrong request of punctis custom api. Missing required param: user email = '.
            $userEmail. ' and type = '.$type, $this);

        //redirect to home in case of missing param
        $this->getResponse()->setRedirect( Mage::getBaseUrl(), 400);
    }
}