<?php
 /**
  * Class     Data.php
  * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_PunctisCustomization_Helper_Data extends Mage_Core_Helper_Abstract{

    const CONFIG_PATH_REQUIRED_KEY = 'punctis_engagement/barilla_custom/segmentation_key_enble';
    const CONFIG_PATH_KEY = 'punctis_engagement/barilla_custom/segmentation_key';

    /**
     * Check if the segmentation route require a key to be used
     * @return bool
     */
    public function isSegmentationRouteKeyRequired(){
        return Mage::getStoreConfigFlag(self::CONFIG_PATH_REQUIRED_KEY);
    }

    /**
     * Get the segmentation route key, return null if the key is not required
     * @return string|null
     */
    public function getSegmentationRouteKey(){
        if($this->isSegmentationRouteKeyRequired()){
            return Mage::getStoreConfig(self::CONFIG_PATH_KEY);
        }
        return null;
    }

    /**
     *
     * Check the validity of the key in input
     * @param $key string
     *
     * @return bool
     */
    public function checkSegmentationRouteKey($key){
        $segmentationKey = $this->getSegmentationRouteKey();

        if($segmentationKey && $key != $this->getSegmentationRouteKey()){
            return false;
        }
        return true;
    }
} 