<?php
 /**
  * Class     MgmType.php
  * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_PunctisCustomization_Model_Attribute_Source_MgmType extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {


    const OPTION_NAME = 'punctis_custom_mgm_type';

    const VALUE_INVITANTE = 1;
    const VALUE_INVITATO = 2;

    const TEXT_VALUE_INVITANTE = 'MGM-INVITANTE';
    const TEXT_VALUE_INVITATO = 'MGM-INVITATO';

    public static function getInviteValueByCode($type){
        $value = null;
        switch($type){
            case $type == self::TEXT_VALUE_INVITANTE :
                $value = self::VALUE_INVITANTE;
                break;
            case $type == self::TEXT_VALUE_INVITATO:
                $value =  self::VALUE_INVITATO;
                break;
            default:
                Mage::throwException('Type not found for segmentation');
        }
        return $value;
    }

    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array(
                array(
                    'label' => '',
                    'value' => ''
                ),
                array(
                    'label' => Mage::helper('bitbull_punctiscustomization')->__('MGM - Invitante'),
                    'value' => self::VALUE_INVITANTE
                ),
                array(
                    'label' => Mage::helper('bitbull_punctiscustomization')->__('MGM - Invitato'),
                    'value' => self::VALUE_INVITATO
                ),
            );
        }
        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option['value']] = $option['label'];
        }
        return $_options;
    }
}