Bitbull PunctisCustomization Extension
=====================
Custom api for punctis

Facts
-----
- version: 1.0.0
- extension key: Bitbull_PunctisCustomization
- [extension on BitBucket](https://bitbucket.org/bitbull/magento-punctis-custom-api)

Description
-----------
New action to get the segmentation of the customer:

    punctisCustomization/api/customerSegmentation

the action need the POST method and accepted three params:

* email
* type
* key

_email_ of the user

_type_ can be one of these:

* MGM-INVITANTE
* MGM-INVITATO

_Key_ can be setted by admin confing, under system->configuration-> punctis - Engagement-> Barilla customization. It is possible configurate:

*Enable Segmentation Api Key, to ask the key for every request
*Segmentation Api Key, to set the key value

The default configuration setted the key as required with value "punct15-4p1-k3y"

The action give the possibility to edit a custom attibute named _MGM type_ added to permit the segmentation of the customers

Copyright
---------
(c) 2016 Bitbull
