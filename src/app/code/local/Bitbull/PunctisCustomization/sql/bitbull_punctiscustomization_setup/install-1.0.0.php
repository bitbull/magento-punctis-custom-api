<?php
/** @var  Mage_Customer_Model_Entity_Setup $installer */
$installer = Mage::getResourceModel('customer/setup', 'customer_setup');

$installer->startSetup();

$installer->addAttribute('customer', Bitbull_PunctisCustomization_Model_Attribute_Source_MgmType::OPTION_NAME, array(
        'label'        => 'MGM type',
        'visible'      => false,
        'required'     => false,
        'type'         => 'int',
        'input'        => 'select',
        'source'       => 'bitbull_punctiscustomization/attribute_source_mgmType',
        'position'     => 1001
    ));

Mage::getSingleton('eav/config')
    ->getAttribute('customer', Bitbull_PunctisCustomization_Model_Attribute_Source_MgmType::OPTION_NAME)
    ->setData('used_in_forms', array('adminhtml_customer'))
    ->save();


$installer->updateAttribute(
    'customer',
    Bitbull_PunctisCustomization_Model_Attribute_Source_MgmType::OPTION_NAME,
    'is_used_for_customer_segment',
    '1'
);


$installer->endSetup();